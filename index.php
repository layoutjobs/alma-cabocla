<!DOCTYPE HTML>
<html lang="pt-br">

<head>
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Cachaça Artesanal Alma Cabocla – O sabor do interior com perfume e delicadeza, uma degustação única para ser sentida de corpo e alma. " />
    <meta http-equiv="content-language" content="pt-br">
    <meta name="keywords" content="Cachaça Artesanal, Alma Cabocla">
    
    <!-- OG Description -->
    <meta property="og:title" content="Cachaça Artesanal Alma Cabocla"/>
    <meta property="og:type" content="website">
    <meta property="og:description" content="O sabor do interior com perfume e delicadeza, uma degustação única para ser sentida de corpo e alma.">
    <meta property="og:image" content="http://almacabocla.com.br/assets/img/logo-og-title.png">


    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/main.css">

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="assets/img/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/img/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/img/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/img/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/img/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/img/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/img/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="assets/img/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-16x16.png">
    <link rel="manifest" href="assets/img/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/img/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <title>Alma Cabocla - Cachaça Artesanal</title>
</head>

<body>
    <!-- quick buy -->
    <div id="quick-buy"> <br>
        <span class="facebook"><a href=""><img src="assets/img/icon-facebook.png" alt="facebook"></a></span>
        <span class="cart"><a href="#comprar" uk-scroll><img src="assets/img/icon-mercado.png" alt="carrinho"></a></span>
    </div>

    <!-- Navbar -->
    <header>
        <nav class="uk-navbar-container uk-margin" uk-sticky="bottom: #offset" uk-navbar>
            <div class="uk-navbar-left">
                <a class="uk-navbar-item uk-logo" href="">
                    <img src="assets/img/logo.png" alt="Logo">
                </a>
            </div>
            <div class="uk-navbar-center">
                <ul class="uk-navbar-nav uk-visible@l">
                    <li><a href="#cachaca" uk-scroll>Cachaça</a></li>
                    <li><a href="#ingredientes" uk-scroll>Ingredientes</a></li>
                    <li><a href="#diferencial" uk-scroll>Diferencial</a></li>
                    <li><a href="#comprar" uk-scroll>Comprar</a></li>
                    <!-- Modal Indique um amigo -->
                    <li><a href="#modal-example" uk-toggle>Indique um Amigo</a></li>
                </ul>
                <a class="uk-navbar-toggle uk-hidden@l" uk-navbar-toggle-icon uk-toggle="target: #offcanvas-nav-primary"></a>
            </div>
        </nav>

        <div id="modal-example" uk-modal>
            <div class="uk-modal-dialog uk-modal-body">
                <h2 style="text-align:center" class="uk-modal-title">Indique Para um Amigo</h2>
                <form class="formphp" name="form" action="recomendar.php" method="post">
                    <div class="uk-margin">
                        <input id="input-modal" class="uk-input uk-width-1-1" type="text" name="seu-nome" placeholder="Seu Nome"
                            required>
                    </div>
                    <div class="uk-margin">
                        <input id="input-modal" class="uk-input uk-width-1-1" type="text" name="nome-amigo" placeholder="Nome do Seu Amigo"
                            required>
                    </div>
                    <div class="uk-margin">
                        <input id="input-modal" class="uk-input uk-width-1-1" type="text" name="email-amigo"
                            placeholder="E-mail Do Seu Amigo" required>
                    </div>
                    <div class="uk-text-center">
                        <button id="indique-enviar" class="uk-button uk-button-primary" type="submit">Enviar</button>
                    </div>
                </form>
            </div>
        </div>
    </header>

    <main>
        <!-- banner-topo -->
        <section id="banner-topo">
            <div class="uk-container uk-text-center uk-container-medium">
                <div class="uk-text-center" uk-grid>
                    <ul class="uk-list">
                        <li>
                            <h1>Sinta o aroma. Sinta o sabor. Sinta sua alma. </h1>
                        </li>
                        <li><img src="assets/img/logo-banner.png" alt="logo-banner"></li>
                    </ul>
                </div>
            </div>
        </section>

        <!-- a cachaça -->
        <section id="cachaca">
            <div class="uk-container uk-container-medium">
                <div class="uk-child-width-1-2@s uk-child-width-1-2@m uk-text-left" uk-grid>
                    <div>
                        <div class="uk-card">
                            <h2>Sabor forte. Aroma marcante. <br> Uma desgustação que marca <br> o momento e a alma.</h2>
                            <p>Alma Cabloca é uma cachaça da terra. Do interior. Que traz a força da cabloca em seu
                                sabor e aroma. Uma cachaça para quem procura momentos marcantes entre amigos ou consigo
                                mesmo. Uma cachaça que esquenta o coração e alivia a alma. Alma Cabloca é uma cachaça
                                artesanal ouro, criada para personalidades fortes. Uma bebida para aqueles que estão
                                nesse mundão para fazer a diferença.</p>
                            <a class="uk-button uk-button-default" uk-scroll href="#ingredientes">INGREDIENTES</a>
                        </div>
                    </div>
                    <div>
                        <div class="uk-card">
                            <img id="garrafa-alma-cabocla" src="assets/img/garrafa.png" alt="garrafa-alma-cabocla">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- ingredientes -->
        <section id="ingredientes">
            <div class="uk-container uk-container-medium">
                <div class="uk-child-width-1-2@s uk-child-width-1-2@m uk-text-left" uk-grid>
                    <div>
                        <div class="uk-card">
                            <img id="block-image-cana" src="assets/img/cana.png" alt="cana-alma-cabocla">
                        </div>
                    </div>
                    <div>
                        <div class="uk-card">
                            <h2>Cana de Minas. Água tratada e muita dedicação. Esses são os segredos do <br>
                                sabor da Alma. </h2>
                            <p>Os ingredientes são selecionados e tratados com muita dedicação e empenho
                                até se tornarem a cachaça. Por isso Alma Cabocla possui um sabor do mato, da terra e do
                                trabalho. Uma cachaça pura no sabor e na alma. </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- diferencial -->
        <section id="diferencial">
            <div class="uk-container uk-container-medium">
                <div class="uk-child-width-1-2@s uk-child-width-1-2@m uk-text-left" uk-grid>
                    <div>
                        <div class="uk-card">
                            <p>Alma Cabocla é envelhecida em tonéis de carvalho. Por isso sua degustação é única. Alma
                                Cabocla traz em seus sabores, a história do homem do campo, a luta diária pela terra e
                                pela vida. Uma cachaça dedicada ao caboclo e à cabocla que moldaram sua alma sob o sol
                                do campo e os perfumes da cana.</p>
                            <h2>Seu sabor possui a pureza da chuva, <br> a força da terra e a delicadeza <br> da alma
                                cabocla.</h2>
                            <a class="uk-button uk-button-default" uk-scroll href="#comprar">COMPRAR</a>
                        </div>
                    </div>
                    <div>
                        <div class="uk-card">
                            <img id="block-barril" src="assets/img/barril.png" alt="barril-alma-cabocla">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- banner bebida -->
        <section id="banner-mid">
            <div class="uk-container center uk-container-medium">
                <div class="uk-text-center uk-grid uk-grid-stack">
                    <div class="uk-width-1-1 uk-first-column">
                        <img src="assets/img/banner-drinks.png" alt="banner-drink-alma-cabocla">
                    </div>
                </div>
            </div>
        </section>

        <!-- comprar -->
        <section id="comprar">
            <div class="uk-container uk-container-medium">
                <div class="uk-child-width-1-2@s uk-child-width-1-2@m uk-text-left" uk-grid>
                    <div>
                        <br>
                        <div class="uk-card">
                            <img src="assets/img/garrafa_2.png" alt="garrafa-comprar">
                        </div>
                    </div>
                    <div>
                        <div class="uk-card">
                            <h2>Alma brasileira. Alma Cabocla. Uma <br> cachaça com sabor e aroma do <br> interior do
                                Brasil.</h2>
                            <p>A Cachaça Alma Cabocla é uma cachaça para amantes dessa bebida, que faz parte da cultura
                                do Brasil. Uma cachaça para os mais exigentes, como também, para os mais
                                descompromissados. Uma cachaça que vai conquistar sua alma de uma forma simples e pura.</p>
                            <ul class="uk-list">
                                <li><img src="assets/img/cart.png" alt="carrinho-comprar"></li>
                                <li>R$ 62,40</li>
                                <li>
                                    <!-- INICIO FORMULARIO BOTAO PAGSEGURO -->
                                    <form id="btn-pagseguro" action="https://pagseguro.uol.com.br/checkout/v2/cart.html?action=add"
                                        method="post">
                                        <!-- NÃO EDITE OS COMANDOS DAS LINHAS ABAIXO -->
                                        <input type="hidden" name="itemCode" value="51A9DFA05151EF32249D7F897DF1FA96" />
                                        <input type="hidden" name="iot" value="button" />
                                        <input type="image" src="https://stc.pagseguro.uol.com.br/public/img/botoes/pagamentos/205x30-comprar.gif"
                                            name="submit" alt="Pague com PagSeguro - é rápido, grátis e seguro!" />
                                    </form>
                                    <!-- FINAL FORMULARIO BOTAO PAGSEGURO -->
                                </li>
                            </ul>
                            <br>
                            <img src="https://stc.pagseguro.uol.com.br/public/img/banners/seguranca/seguranca_468X60.gif"
                                alt="Banner PagSeguro" title="Compre com pagSeguro e fique sossegado">
                        </div>
                    </div>
                </div>
            </div>
            <br>
        </section>

        <!-- copyright -->
        <section id="copyright">
            <div class="uk-container uk-align-center">
                <p style="font-weight: 300;">Copyright © 2018. Todos os direitos reservados.<a style="font-weight:bold;color:#856534;"
                        href="#modal-example-politica" uk-toggle>Política de Troca e Devoluções.</a></p>
                <div id="modal-example-politica" uk-modal>
                    <div class="uk-modal-dialog uk-modal-body">
                        <h2 class="uk-modal-title">Política de Troca e Devoluções.</h2>
                        <p class="block-modal-troca"><strong>TROCA</strong></p>
                        <p class="block-modal-troca">A SEIVA DA TERRA ORGÂNICOS efetuará a troca do(s) produto(s)
                            adquirido(s)
                            por seus Clientes, de acordo com o previsto no Código de Defesa do Consumidor.
                            A opção de troca do produto somente estará disponível na hipótese de constatação de vício
                            ou
                            defeito, devendo o Cliente enviar email para falecom@almacabocla.com.br e seguir as
                            instruções
                            recebidas para que o pedido de troca seja analisado e processado.
                            Caso identificado vicio de fabricação, o prazo para solicitação de troca/devolução é de até
                            7
                            (sete) dias corridos, contados a partir da data do recebimento do produto.
                            Caso o produto a ser trocado não esteja mais disponível na loja virtual, o Cliente poderá
                            optar
                            pela: <br> <br>

                            <strong>
                                A) Troca por outro produto disponível no site Alma Cabocla, do mesmo valor do produto
                                adquirido, incluindo o frete, caso tenha sido cobrado;<br>
                                B) Restituição do valor do produto, incluindo o valor pago pelo frete.
                            </strong>
                        </p>
                        <br> <br>
                        <p class="block-modal-troca"><strong>DEVOLUÇÃO</strong></p>
                        <p class="block-modal-troca">Por arrependimento da compra (devolução)
                            Caso o Cliente se arrependa de uma compra efetuada, deverá enviar email para
                            falecom@almacabocla, num prazo máximo de (sete) dias corridos contados a partir do
                            recebimento
                            do produto.
                            Os produtos deverão ser enviados em sua embalagem original. Constatado o cancelamento da
                            compra, a SEIVA DA TERRA ORGANICOS adotará as providências e os valores serão restituídos
                            ao
                            Cliente de acordo com o meio de pagamento utilizado para realização da compra, sendo:
                            <br> <br>

                            <strong>A) Depósito em conta corrente, no prazo de até 5 (cinco) dias úteis <br>
                                B) Acionamento da administradora de cartão de crédito, no prazo de 2 (dois) dias úteis,
                                para que ela comunique a instituição financeira responsável pelo crédito (estorno) que
                                poderá ocorrer em até duas faturas subsequentes, conforme a data de fechamento da
                                fatura de
                                cartão de crédito.
                            </strong>
                        </p>
                        <p class="block-modal-troca">
                            Os prazos só serão computados a partir da devolução do(s) produto(s) ao escritório
                            comercial,
                            cujo endereço será informado através de email.
                        </p>
                        <p class="block-modal-troca">Toda TROCA/DEVOLUÇÃO de produtos comercializados pela SEIVA DA
                            TERRA
                            ORGÂNICOS deverá ser
                            comunicada através do email falecom@almacabocla.com.br, antes do envio dos produtos.
                            Só será aceita a troca ou pedido de desistência do produto adquirido pelo Cliente que
                            esteja:
                            <br> <br>
                            <strong>A) Perfeitamente acondicionado na embalagem original; <br>
                                B) Sem indícios de uso ou consumo; <br>
                                C) Acompanhado da 1ª via da Nota Fiscal de Venda ou DANFE (Documento Auxiliar de Nota
                                Fiscal Eletrônica), conforme o caso.
                            </strong>
                        </p>
                        <p class="block-modal-troca">A SEIVA DA TERRA ORGÂNICOS somente efetuará a troca de produtos
                            após
                            o
                            recebimento e a constatação de que o produto está apto para a ser, novamente, revendido sem
                            ônus nenhum para o próximo comprador. Esse processo pode levar até 3 (três) dias úteis a
                            contar
                            do recebimento da mercadoria em questão em nosso escritório comercial.</p>
                    </div>
                </div>
            </div>
            <br>
        </section>
    </main>

    <footer>
        <section id="footer">
            <div>
                <div id="banner-madeira" class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle"
                    style="background-image: url(assets/img/footer.png); height: 100%;">
                    <div id="grid" class="uk-child-width-1-2@s uk-child-width-1-2@m uk-text-center" uk-grid>
                        <div>
                            <div class="uk-card"><img src="assets/img/logo-footer.png" alt="Footer"></div>
                        </div>
                        <div>
                            <div class="uk-card-distribuicao">
                                <p>Produzida e Engarrafada por:
                                    ENGENHO SUL MINEIRO LTDA.
                                    Rodovia BR 459 - KM47,8 - Bairro Gineta I
                                    Santa Rita de Caldas - Minas Gerais
                                    CNPJ: 02.897.966/0001-07
                                    Registro no M.A.P.A.: MG/ 314-0.000017
                                    Envelhecida em Tonéis de Amburana. </p>
                                <p>Distribuição exclusiva:
                                    SEIVA DA TERRA ORGÂNICOS
                                    CNPJ 30.695.843/0001-10
                                    Evite o consumo excessivo de álcool.
                                    Produto para maiores de 18 anos.
                                    INDÚSTRIA BRASILEIRA
                                    NÃO CONTÉM GLÚTEN</p>
                            </div>
                        </div>
                    </div>
                    <br> <br>
                </div>
            </div>
        </section>
    </footer>



    <!-- Menu Responsivo -->
    <div class="uk-offcanvas-content">
        <div id="offcanvas-nav-primary" uk-offcanvas="overlay: true">
            <div class="uk-offcanvas-bar uk-flex uk-flex-column uk-offcanvas-close" style="top: 0px; width: 78%;">
                <a style="margin-top: 17px; text-align: center" class="uk-offcanvas-brand" href="">
                    <img src="assets/img/logo.png" alt="Logo">
                </a>
                <ul class="uk-nav uk-nav-primary uk-nav-center uk-margin-auto-vertical">
                    <li><a href="#cachaca" uk-scroll>Cachaça</a></li>
                    <li><a href="#ingredientes" uk-scroll>Ingredientes</a></li>
                    <li><a href="#diferencial" uk-scroll>Diferencial</a></li>
                    <li><a href="#comprar" uk-scroll>Comprar</a></li>
                    <!-- Modal Indique um amigo -->
                    <li><a href="#modal-example" uk-toggle>Indique um Amigo</a></li>
                    <li class="uk-nav-divider"></li>
                    <li><a href="#"><span class="uk-margin-small-right" uk-icon="icon: facebook"></span> Curta no
                            Facebook</a></li>
                </ul>
            </div>
        </div>
    </div>


    <!-- UIkit JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.25/js/uikit.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.25/js/uikit-icons.min.js"></script>
</body>


</html>